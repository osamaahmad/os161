/*
 * All the contents of this file are overwritten during automated
 * testing. Please consider this before changing anything in this file.
 */

#include <types.h>
#include <lib.h>
#include <clock.h>
#include <thread.h>
#include <synch.h>
#include <test.h>
#include <kern/test161.h>
#include <spinlock.h>

/*
 * Use these stubs to test your reader-writer locks.
 */

static const int THREADS = 16;
struct spinlock status_lock;
static int shared_value = 0;
static struct semaphore *exit_sem = NULL; 
static struct semaphore *read_sem = NULL; 
static struct semaphore *write_sem = NULL; 
static struct rwlock    *rwlock = NULL;

static
void
read_shared_value(void *junk, unsigned long junk2)
{
	(void)junk;
	(void)junk2;
	random_yielder(4);
	P(read_sem);

	rwlock_acquire_read(rwlock);
	
	kprintf("shared value: %d (read)\n", shared_value);

	rwlock_release_read(rwlock);

	V(write_sem);
	V(exit_sem);
}

static
void
write_shared_value(void *junk, unsigned long junk2)
{
	(void)junk;
	(void)junk2;
	random_yielder(4);
	P(write_sem);

	rwlock_acquire_write(rwlock);

	kprintf("shared value: %d \t(write)\n", ++shared_value);

	rwlock_release_write(rwlock);
	V(read_sem);
	V(exit_sem);
}

static
void
waste_time(int loop_count)
{
	for (int i = 0; i < loop_count; i++) {
		struct lock* junk = lock_create("junk");
		lock_destroy(junk);
	}
}

static
void
rwlock_test(const char *name, int read_sem_init, int write_sem_init, int read_threads, int write_threads, int read_loop_count, int write_loop_count)
{
	kprintf_n("Starting %s...\n", name);
	kprintf_n("read_sem  initial value: %d\n", read_sem_init);
	kprintf_n("write_sem initial value: %d\n", write_sem_init);
	kprintf_n("total number of read  threads: %d\n", read_threads);
	kprintf_n("total number of write threads: %d\n", write_threads);
	kprintf_n("read_loop_count : %d\n", read_loop_count);
	kprintf_n("write_loop_count: %d\n", write_loop_count);

	shared_value = 0;

	exit_sem = sem_create("exit", 0);
	if (exit_sem == NULL) {
		panic("exit_sem: sem_create failed\n");
	}

	read_sem = sem_create("read", read_sem_init);
	if (read_sem == NULL) {
		panic("read_sem: sem_create failed\n");
	}

	write_sem = sem_create("write", write_sem_init);
	if (write_sem == NULL) {
		panic("write_sem: sem_create failed\n");
	}

	rwlock = rwlock_create(name);
	if (rwlock == NULL) {
		panic("rwlock: rwlock_create failed\n");
	}

	kprintf_n("If this hangs, it's broken: \n");
	
	for (int i = 0; i < write_threads; i++) {
		waste_time(write_loop_count);

		int result = thread_fork(name, NULL, write_shared_value, NULL, 0);

		if (result) {
			panic("%s: thread_fork failed\n", name);
		}
	}

	for (int i = 0; i < read_threads; i++) {
		waste_time(read_loop_count);

		int result = thread_fork(name, NULL, read_shared_value, NULL, 0);

		if (result) {
			panic("%s: thread_fork failed\n", name);
		}
	}

	for (int i = 0; i < read_threads + write_threads; i++) {
		P(exit_sem);
	}
	
	success(TEST161_SUCCESS, SECRET, name);

	sem_destroy(exit_sem);
	sem_destroy(read_sem);
	sem_destroy(write_sem);
	rwlock_destroy(rwlock);
	
	exit_sem = NULL;
	read_sem = NULL;
	write_sem = NULL;
	rwlock = NULL;
}

int rwtest(int nargs, char **args) {
	(void)nargs;
	(void)args;

	rwlock_test("rwt1", 100, 20, 100, 20, 0, 0);
	
	return 0;
}

int rwtest2(int nargs, char **args) {
	(void)nargs;
	(void)args;

	rwlock_test("rwt2", 50, 0, 50, 0, 0, 0);

	return 0;
}

int rwtest3(int nargs, char **args) {
	(void)nargs;
	(void)args;

	kprintf_n("Starting rwt3...\n");
	kprintf_n("(This test panics on success!)\n");

	rwlock = rwlock_create("rwt3");
	if (rwlock == NULL) {
		panic("rwt3: rwlock_create failed");
	}

	secprintf(SECRET, "Should panic...", "rwt3");
	rwlock_release_read(rwlock);

	/* Should not get here on success. */

	success(TEST161_FAIL, SECRET, "rwt3");

	/* Don't do anything that could panic. */
	
	rwlock_destroy(rwlock);

	rwlock = NULL;

	return 0;
}

int rwtest4(int nargs, char **args) {
	(void)nargs;
	(void)args;

	kprintf_n("Starting rwt4...\n");
	kprintf_n("(This test panics on success!)\n");

	rwlock = rwlock_create("rwt4");
	if (rwlock == NULL) {
		panic("rwt4: rwlock_create failed");
	}

	secprintf(SECRET, "Should panic...", "rwt4");
	rwlock_release_write(rwlock);

	/* Should not get here on success. */

	success(TEST161_FAIL, SECRET, "rwt4");

	/* Don't do anything that could panic. */
	
	rwlock_destroy(rwlock);

	rwlock = NULL;

	return 0;
}

int rwtest5(int nargs, char **args) {
	(void)nargs;
	(void)args;

	kprintf_n("Starting rwt5...\n");
	kprintf_n("(This test panics on success!)\n");

	rwlock = rwlock_create("rwt5");
	if (rwlock == NULL) {
		panic("rwt5: rwlock_create failed");
	}

	secprintf(SECRET, "Should panic...", "rwt5");

	rwlock_acquire_write(rwlock);
	rwlock_destroy(rwlock);

	/* Should not get here on success. */

	success(TEST161_FAIL, SECRET, "rwt5");

	/* Don't do anything that could panic. */
	
	rwlock = NULL;

	return 0;
}
