#include <proc_operations.h>
#include <kern/fcntl.h>
#include <kern/errno.h>
#include <copyinout.h>
#include <addrspace.h>
#include <kern/wait.h>
#include <current.h>
#include <syscall.h>
#include <thread.h>
#include <wchan.h>
#include <proc.h>
#include <vfs.h>

extern struct spinlock thread_count_lock;
extern struct wchan *thread_count_wchan;

// used by exec, shared between all threads
// instead of each thread having its own.
char stack[_ARG_MAX];
char kprogram[_PATH_MAX];
// acquired while using the shared resources
// so that only one thread is using them at
// a time.
struct lock *exec_lock;

int sys_fork(struct trapframe *s_tf, pid_t *ret_pid)
{
	// when to return EMPROC?
	// TODO make sure to understand exactly what each function does.

	int result;

	struct trapframe *tf;
	tf = kmalloc(sizeof(*tf));
	if (tf == NULL) {
		return ENOMEM;
	}

	// TODO use a better name.
	struct proc *new_proc = proc_create_runprogram("forked proc");
	if (new_proc == NULL) {
		kfree(tf);
		return ENOMEM; 
	}

	if (new_proc->pid == -1) {
		proc_destroy(new_proc);
		kfree(tf);
		return ENPROC;
	}

	struct addrspace *as;
	result = as_copy(curproc->p_addrspace, &as);
	if (result) {
		proc_destroy(new_proc);
		kfree(tf);
		return result;
	}

	struct filetable *ft = filetable_copy(curproc->ft);
	if (ft == NULL) {
		as_destroy(as);
		proc_destroy(new_proc);
		kfree(tf);
		return ENOMEM;
	}

	*tf = *s_tf;
	new_proc->p_addrspace = as;
	new_proc->ft = ft;
	new_proc->parent = curproc;

	void (*entry)(void *, long unsigned int) = 
		(void (*)(void *, long unsigned int))enter_forked_process;

	// TODO use a better name.
	result = thread_fork("forked thread", new_proc, entry, (void *)tf, 0);
	if (result) {
		// as and ft will be destroyed
		// from within proc_destroy.
		proc_destroy(new_proc);
		kfree(tf);
		return result;
	}

	*ret_pid = new_proc->pid;

	return 0;
}

static int copy_argv_pointers(char *stack, userptr_t args, size_t *stack_size_ptr, int *argc)
{
	int i = 0;
	int result;
	size_t stack_size = 0;

	while (true) {

		char     *dest_addr   = (char    *)((char **)stack + i);
		userptr_t source_addr = (userptr_t)((char **)args  + i);

		result = copyin(source_addr, dest_addr, sizeof(char *));
		if (result) {
			return result;
		}

		stack_size += sizeof(char *);

		if (((char **)stack)[i] == NULL) {
			break;
		}

		i++;
	}

	*argc = i;
	*stack_size_ptr = stack_size;

	return 0;
}

static int set_argv(char *stack, size_t *stack_size_ptr, int argc)
{
	int result;
	size_t stack_size = *stack_size_ptr;

	// kprintf("argc: %d, arguments:\n", argc);

	size_t len;
	for (int i = 0; i < argc; i++) {

		userptr_t source  = (userptr_t)((char **)stack)[i];
		char     *dest    = stack + stack_size;
		int       max_len = _ARG_MAX - stack_size;
		
		// copystr (called from copyinstr) includes the null 
		// terminator in the lenght it sets.
		result = copyinstr(source, dest, max_len, &len);
		if (result) {
			return result;
		}

		// kprintf("%s\n", dest);

		int mod = len % ALIGNMENT_SIZE;
		int remaining = (mod ? ALIGNMENT_SIZE - mod : 0);
		for (int j = 0; j < remaining; j++) {
			dest[len + j] = 0;
		}

		len += remaining;

		// this is just the offset from the top of the stack pointer.
		// the value of the stack pointer will be added later to each address.
		// the actual stack size will be subtracted after knowing it.
		((char **)stack)[i] = (char *)stack_size;
		stack_size += len;
	}

	for (int i = 0; i < argc; i++) {
		((char **)stack)[i] -= stack_size;
	}

	*stack_size_ptr = stack_size;

	return 0;
}

static void add_stackptr_to_argv(char **argv, int argc, char *stackptr)
{
	for (int i = 0; i < argc; i++) {

		// size_t? off_t is 64 bits.
		argv[i] += (size_t)stackptr;

		KASSERT((size_t)(argv[i]) % ALIGNMENT_SIZE == 0);
	}
}

int sys_execv(const_userptr_t program, userptr_t args)
{
	int result;
	
	size_t proglen;

	lock_acquire(exec_lock);

	result = copyinstr(program, kprogram, _PATH_MAX, &proglen);
	if (result) {
		lock_release(exec_lock);
		return result;
	}

	int argc;
	size_t stack_size = 0;
	
	// copy the content of args in stack.
	result = copy_argv_pointers(stack, args, &stack_size, &argc);
	if (result) {
		lock_release(exec_lock);
		return result;
	}

	// copy the actual strings and change the values
	// of the pointers to point to the right position
	// in the user stack.
	result = set_argv(stack, &stack_size, argc);
	if (result) {
		lock_release(exec_lock);
		return result;
	}

	KASSERT(stack_size % ALIGNMENT_SIZE == 0);

	lock_release(exec_lock);

	struct vnode *v;
	result = vfs_open(kprogram, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	KASSERT(curproc != NULL);
	struct addrspace *old_as = curproc->p_addrspace;

	struct addrspace *as;
	as = as_create();
	if (as == NULL) {
		vfs_close(v);
		result = ENOMEM;
		goto fail;
	}

	proc_setas(as);
	as_activate();

	vaddr_t entrypoint;
	result = load_elf(v, &entrypoint);

	vfs_close(v);

	if (result) {
		goto fail;
	}

	vaddr_t stackptr;
	result = as_define_stack(as, &stackptr);
	if (result) {
		goto fail;
	}

	add_stackptr_to_argv((char **)stack, argc, (char *)stackptr);

	userptr_t user_stack_top = (userptr_t)stackptr - stack_size;

	result = copyout(stack, user_stack_top, stack_size);
	if (result) {
		goto fail;
	}

	as_destroy(old_as);

	enter_new_process(argc, user_stack_top, NULL /* env */, 
							stackptr - stack_size, entrypoint);

	panic("enter_new_process returned");

	return EINVAL;

	fail:
	as_destroy(as);
	proc_setas(old_as);
	as_activate();
	return result;
}

int sys_waitpid(pid_t pid, userptr_t status, int options, int *ret_pid)
{
	// only supports one (the parent) to wait on 
	// a process since it'll cleanup right after
	// collecting the exit status without checking
	// whether there are other processes that are
	// waiting or not.

	if (options) {
		return EINVAL;
	}

	// TODO using the lock for a simple
	// task. Inefficient.
	lock_acquire(proctable->lock);

	if (pid < __PID_MIN || pid > __PID_MAX) {
		lock_release(proctable->lock);
		// TODO make sure of this return value.
		return ECHILD;
	}

	struct proc *child = proctable->table[pid];

	lock_release(proctable->lock);

	if (child == NULL) {
		return ESRCH;	
	}
	
	if (child->parent != curproc) {
		return ECHILD;
	}

	spinlock_acquire(&thread_count_lock);

	// not so efficient but thats what thread_exit supports.
	// you may write another version of thread_exit to make
	// it so that each process can have its own lock.
	while (!(child->exited)) {
		wchan_sleep(thread_count_wchan, &thread_count_lock);
	}
		
	spinlock_release(&thread_count_lock);

	if (status != NULL) {
		int *ptr = &(child->exitstatus);
		int result = copyout(ptr, status, sizeof(*ptr));
		if (result) {
			return result;
		}
	}

	proc_destroy(child);

	*ret_pid = pid;

	return 0;
}

void _sys__exit(int exitstatus)
{
	// TODO what happens to the children?
	// TODO should some internal structures be destroyed (i.e. filetable)?

	// for now, only the parent will wait for its child.

	// supports only single threaded programs since it relies on the 
	// thread_exit to set the process "exited" to true whenever a thread
	// exits. you might change the code if support for multithreading is needed.

	curproc->exitstatus = exitstatus;
	thread_exit();
}

void sys__exit(int exitcode)
{
	_sys__exit(_MKWAIT_EXIT(exitcode));
}

int sys_getpid(int *ret_pid)
{
	*ret_pid = curproc->pid;
	return 0;
}
