#include <file_operations.h>
#include <vfs.h>
#include <kern/errno.h>
#include <proc.h>
#include <synch.h>
#include <current.h>
#include <copyinout.h>
#include <uio.h>
#include <kern/fcntl.h>
#include <kern/seek.h>
#include <kern/stat.h>

int sys_open_nocheck(struct filetable *ft, char *filename, int flags, mode_t mode, int *ret_fd)
{
	int result;

	struct filehandle *fh = filehandle_create();
	if (fh == NULL) {
		// TODO make sure of this return value.
		return ENOMEM;
	}

	result = vfs_open(filename, flags, mode, &(fh->vnode_ptr));
	if (result) {
		filehandle_destroy(fh);
		return result;
	}

	// only the last two bits.
	fh->flags = flags & 0x3;
	
	if (VOP_ISSEEKABLE(fh->vnode_ptr)) {
		fh->flags |= FILEHANDLE_ISSEEKABLE;
	}

	int fd;

	filetable_lock(ft);

	result = get_min_available_fd(ft, &fd);
	if (result) {
		filetable_unlock(ft);
		filehandle_destroy(fh);
		vfs_close(fh->vnode_ptr);
		return result;
	}

	ft->table[fd] = fh;

	filetable_unlock(ft);

	// ret_fd is coming from the kernel space.
	// no need to check the pointer.
	*ret_fd = fd;

	return 0;
}

int sys_open(userptr_t filename, int flags, mode_t mode, int *ret_fd)
{
	int result;
	size_t len;
	char filename_buf[PATH_MAX];

	result = copyinstr(filename, filename_buf, PATH_MAX, &len);
	if (result) {
		return result;
	}

	return sys_open_nocheck(curproc->ft, filename_buf, flags, mode, ret_fd);
}

// the table should be locked before calling this function.
void sys_close_nocheck_nolock(struct filetable *ft, int fd)
{
	struct filehandle **handles = ft->table;

	// note that it'll never happen that
	// the filehandle gets copied in an another
	// thread while being destroyed here since
	// it'll only get destroyed if this is
	// the last reference, meaning that no
	// other slot in the filetable is pointing
	// to the this filehanle. this fact combined
	// with the fact that each slot is locked 
	// before accessing it, indicates that 
	// this thread has the only reference and
	// no other thread would be able to access
	// this file handle until this call finishes.
	// in case the filehandle here is destroyed,
	// the lock won't be released until the
	// filehanlde is completely destroyed and 
	// the slot containing it nulled out.

	// decref is atomic and returns the new
	// value. it has to be atomic so that only
	// one thread ends up calling filehandle_destroy.
	if (filehandle_decref(handles[fd]) == 0) {
		filehandle_destroy(handles[fd]);
	}

	handles[fd] = NULL;
	reclaim_fd(ft, fd);
}

int sys_close(int fd)
{
	// when to return EIO?..
	
	struct filetable *ft  = curproc->ft;

	if (fd < 0 || fd >= ft->capacity) {
		return EBADF;
	}

	filetable_lock(ft);

	if (ft->table[fd] == NULL) {
		filetable_unlock(ft);
		return EBADF;
	}

	sys_close_nocheck_nolock(ft, fd);
	
	filetable_unlock(ft);

	return 0;
}

int sys_write_nocheck(struct filehandle *fh, void *buf, size_t buflen, ssize_t *written)
{
	struct iovec iov;
	struct uio uio;

	int result;

	lock_acquire(fh->offset_lock);
	
	// fh->offset has to be accessed from within the critical area.
	uio_kinit(&iov, &uio, buf, buflen, fh->offset, UIO_WRITE);
result = VOP_WRITE(fh->vnode_ptr, &uio);

	if (result) {
		return result;
	}

	*written = (buflen - uio.uio_resid);

	fh->offset = uio.uio_offset;

	lock_release(fh->offset_lock);

	return 0;
}

int sys_write(int fd, const_userptr_t buf, size_t buflen, ssize_t *written)
{
	struct filetable *ft = curproc->ft;

	if (fd < 0 || fd >= ft->capacity) {
		return EBADF;
	}

	filetable_lock_handle(ft, fd);

	int write_flags = (O_WRONLY | O_RDWR);
	struct filehandle **handles = ft->table;

	if (handles[fd] == NULL || !(handles[fd]->flags & write_flags)) {
		filetable_unlock_handle(ft, fd);
		return EBADF;
	}

	if (buflen == 0) {
		filetable_unlock_handle(ft, fd);
		return 0;
	}

	int result;

	void *kbuf = kmalloc(buflen);
	if (kbuf == NULL) {
		filetable_unlock_handle(ft, fd);
		return ENOMEM;
	}

	// do we copy till the end on each 
	// call if sys_write_nocheck didn't
	// write the whole buffer?
	// maybe there is a better solution.
	result = copyin(buf, kbuf, buflen);
	if (result) {
		filetable_unlock_handle(ft, fd);
		kfree(kbuf);
		return result;
	}

	result = sys_write_nocheck(handles[fd], kbuf, buflen, written);

	filetable_unlock_handle(ft, fd);

	kfree(kbuf);

	return result;
}

int sys_read_nocheck(struct filehandle *fh, void *buf, size_t buflen, ssize_t *read)
{
	struct iovec iov;
	struct uio uio;

	int result;

	lock_acquire(fh->offset_lock);

	// fh->offset has to be accessed from within the critical area.
	uio_kinit(&iov, &uio, buf, buflen, fh->offset, UIO_READ);

	result = VOP_READ(fh->vnode_ptr, &uio);

	if (result) {
		return result;
	}

	*read = (buflen - uio.uio_resid);

	fh->offset = uio.uio_offset;

	lock_release(fh->offset_lock);

	return 0;
}

static bool is_opened_for_read(int flags)
{
	return !(flags & O_WRONLY) || (flags & O_RDWR);
}

int sys_read(int fd, userptr_t buf, size_t buflen, ssize_t *read)
{
	struct filetable *ft = curproc->ft;

	if (fd < 0 || fd >= ft->capacity) {
		return EBADF;
	}

	filetable_lock_handle(ft, fd);

	struct filehandle **handles = ft->table;

	// O_RDONLY = 0 and O_WRONLY = 1. if O_WRONLY
	// is set, then O_WRONLY is not set.
	if (handles[fd] == NULL || !is_opened_for_read(handles[fd]->flags)) {
		filetable_unlock_handle(ft, fd);
		return EBADF;
	}

	if (buflen == 0) {
		filetable_unlock_handle(ft, fd);
		return 0;
	}

	int result;

	void *kbuf = kmalloc(buflen);
	if (kbuf == NULL) {
		filetable_unlock_handle(ft, fd);
		return ENOMEM;
	}

	result = sys_read_nocheck(handles[fd], kbuf, buflen, read);

	filetable_unlock_handle(ft, fd);
	
	// in case *read == 0, copycheck fails and
	// returns EFAULT. not necessary anyways
	// to try to read 0 bytes.
	if (result || *read == 0) {
		goto finish;
	}

	result = copyout(kbuf, buf, *read);

	finish:
	
	kfree(kbuf);

	return result;
}

int sys_lseek_nocheck(struct filehandle *fh, off_t pos, int whence, off_t *ret_offset)
{
	off_t new_offset;

	switch (whence) {

		case SEEK_SET:
			new_offset = pos;
			break;

		case SEEK_CUR:
			new_offset = fh->offset + pos;
			break;

		case SEEK_END:; 
			// ; is put to have the lable point
			// to a statement since declarations
			// are not considered a statement in c.
			struct stat statbuf;
			// VOP_STAT can be called outside the 
			// critical area, but that would add
			// its overhead to each call even if
			// it's not needed for this operation.
			VOP_STAT(fh->vnode_ptr, &statbuf);
			new_offset = statbuf.st_size + pos;
			break;

		default:
			return EINVAL;
	}

	if (new_offset < 0) {
		return EINVAL;
	}

	// TODO Inefficient...
	lock_acquire(fh->offset_lock);
	fh->offset = new_offset;
	lock_release(fh->offset_lock);

	*ret_offset = new_offset;


	return 0;
}

static void split_64bits_to_two_32bits(int64_t input, int32_t *ret_high_bits, int32_t *ret_low_bits)
{
	uint64_t u_input = (uint64_t)input;

	uint64_t low_bits_mask = (uint32_t)-1;
	uint64_t high_bits_mask = ~low_bits_mask;

	uint64_t low_bits  =  u_input & low_bits_mask;
	uint64_t high_bits = (u_input & high_bits_mask) >> 32;

	*ret_high_bits = (int32_t)high_bits;
	*ret_low_bits  = (int32_t)low_bits;
}

static int64_t merge_two_32bits_to_64bits(int32_t high_bits, int32_t low_bits)
{
	uint64_t u_res = (uint32_t)high_bits;
	u_res <<= 32;
	u_res |= (uint32_t)low_bits;
	return (int64_t)u_res;
}

int sys_lseek(int fd, int32_t pos_high_bits, int32_t pos_low_bits,
		 userptr_t whence, int32_t *ret_offset_high_bits, int32_t *ret_offset_low_bits)
{
	struct filetable *ft = curproc->ft;

	if (fd < 0 || fd >= ft->capacity) {
		return EBADF;
	}

	filetable_lock_handle(ft, fd);

	if (curproc->ft->table[fd] == NULL) {
		filetable_unlock_handle(ft, fd);
		return EBADF;
	}

	struct filehandle *fh = ft->table[fd];

	if (!(fh->flags & FILEHANDLE_ISSEEKABLE)) {
		filetable_unlock_handle(ft, fd);
		return ESPIPE;
	}

	int result;

	int kwhence;
	result = copyin(whence, &kwhence, sizeof(kwhence));
	if (result) {
		filetable_unlock_handle(ft, fd);
		return result;
	}

	off_t new_offset;
	off_t pos = merge_two_32bits_to_64bits(pos_high_bits, pos_low_bits);

	result = sys_lseek_nocheck(fh, pos, kwhence, &new_offset);

	filetable_unlock_handle(ft, fd);

	if (result) {
		return result;
	}

	split_64bits_to_two_32bits((uint64_t)new_offset, 
				ret_offset_high_bits, ret_offset_low_bits);

	return 0;
}

int sys_dup2_nocheck(struct filetable *ft, int oldfd, int newfd)
{
	// TODO nocheck? really?..

	filetable_lock(ft);

	struct filehandle *fh = ft->table[oldfd];

	if (fh == NULL) {
		filetable_unlock(ft);
		return EBADF;
	}
		
	if (oldfd == newfd) {
		filetable_unlock(ft);
		return 0;
	}

	if (ft->table[newfd] != NULL) {
		sys_close_nocheck_nolock(ft, newfd);
	}

	ft->table[newfd] = fh;
	filehandle_incref(fh);

	filetable_unlock(ft);

	return 0;
}

int sys_dup2(int oldfd, int newfd, int *ret_fd)
{
	// when to return EMFILE or ENFILE?...

	struct filetable *ft = curproc->ft;

	if (oldfd < 0 || newfd < 0 || 
		oldfd >= ft->capacity || newfd >= ft->capacity) {
		return EBADF;
	}

	int result = sys_dup2_nocheck(ft, oldfd, newfd);
	if (result) {
		return result;
	}

	*ret_fd = newfd;

	return 0;
}

int sys_chdir(userptr_t pathname)
{
	size_t len;
	char kpathname[PATH_MAX];

	int result;

	result = copyinstr(pathname, kpathname, PATH_MAX, &len);
	if (result) {
		return result;
	}
	
	return vfs_chdir(kpathname);
}

int sys___getcwd(userptr_t buf, size_t len, int *actual_len)
{
	struct iovec iov;
	iov.iov_ubase = buf;
	iov.iov_len = len;

	struct uio uio;
	uio.uio_iov = &iov; 
	uio.uio_iovcnt = 1;
	uio.uio_offset = 0;
	uio.uio_resid = len;
	uio.uio_segflg = UIO_USERSPACE;
	uio.uio_rw = UIO_READ;
	uio.uio_space = proc_getas();

	int result = vfs_getcwd(&uio);
	if (result) {
		return result;
	}

	*actual_len = uio.uio_offset;

	return 0;
}
