#include <types.h>
#include <lib.h>
#include <spinlock.h>
#include <synch.h>
#include <vnode.h>
#include <filetable.h>
#include <kern/errno.h>
#include <file_operations.h>
#include <vfs.h>

// TODO consider using types other than int for loops.

struct filehandle *filehandle_create()
{
	struct filehandle *fh;
	fh = kmalloc(sizeof(*fh));

	if (fh == NULL) {
		return NULL;
	}

	fh->offset_lock = lock_create("offset_lock");
	if (fh->offset_lock == NULL) {
		kfree(fh);
		return NULL;
	}

	spinlock_init(&(fh->refcount_spinlock));

	fh->refcount = 1;
	fh->offset = 0;
	fh->vnode_ptr = NULL;
	fh->flags = 0;

	return fh;
}

void filehandle_destroy(struct filehandle *fh)
{	
	//TODO check the cleaning and freeing.

	KASSERT(fh != NULL);
	KASSERT(fh->offset_lock != NULL);

	// probably not necessary.
	// KASSERT(fh->refcount == 0);

	if (fh->vnode_ptr != NULL) {
		vfs_close(fh->vnode_ptr);
	}

	lock_destroy(fh->offset_lock);
	spinlock_cleanup(&(fh->refcount_spinlock));

	kfree(fh);
}

void filehandle_incref(struct filehandle *fh)
{
	spinlock_acquire(&(fh->refcount_spinlock));

	KASSERT(fh->refcount > 0);
	fh->refcount++;

	spinlock_release(&(fh->refcount_spinlock));
}

int filehandle_decref(struct filehandle *fh)
{
	spinlock_acquire(&(fh->refcount_spinlock));

	KASSERT(fh->refcount > 0);
	fh->refcount--;
	int result = fh->refcount;

	spinlock_release(&(fh->refcount_spinlock));

	return result;
}

struct filetable *filetable_create()
{
	struct filetable *ft;
	ft = kmalloc(sizeof(*ft));

	if (ft == NULL) {
		return NULL;
	}

	ft->capacity = FILETABLE_SIZE;

	ft->table = kmalloc(ft->capacity * sizeof(struct filehandle *));
	if (ft->table == NULL) {
		kfree(ft);
		return NULL;
	}

	ft->fh_locks = kmalloc(ft->capacity * sizeof(struct lock *));
	if (ft->fh_locks == NULL) {
		kfree(ft->table);
		kfree(ft);
		return NULL;
	}

	// TODO use custom names for easier debugging.
	ft->table_lock = rwlock_create("table_lock");
	if (ft->table_lock == NULL) {
		kfree(ft->table);
		kfree(ft->fh_locks);
		kfree(ft);
		return NULL;
	}

	for (int i = 0; i < ft->capacity; i++) {
		// TODO use custom names for easier debugging.
		ft->fh_locks[i] = lock_create("fh_lock");
		if (ft->fh_locks[i] == NULL) {

			for (int j = 0; j < i; j++) {
				lock_destroy(ft->fh_locks[j]);
			}

			rwlock_destroy(ft->table_lock);
			kfree(ft->table);
			kfree(ft->fh_locks);
			kfree(ft);
			return NULL;
		}
	}

	//TODO may use a function here (i.e. bzero)
	for (int i = 0; i < ft->capacity; i++) {
		ft->table[i] = NULL;
	}

	spinlock_init(&(ft->ssi_spinlock));

	ft->search_start_index = 0;

	return ft;
}

void filetable_destroy(struct filetable *ft)
{
	KASSERT(ft != NULL);
	KASSERT(ft->table != NULL);
	KASSERT(ft->table_lock != NULL);

	// TODO is this necessary?
	filetable_lock(ft);

	for (int i = 0; i < ft->capacity; i++) {
		if (ft->table[i] != NULL) {
			sys_close_nocheck_nolock(ft, i);
		}
	}

	filetable_unlock(ft);

	kfree(ft->table);
	rwlock_destroy(ft->table_lock);

	// TODO why only locks are allowed to be NULL?
	if (ft->fh_locks != NULL) {
		for (int i = 0; i < ft->capacity; i++) {
			lock_destroy(ft->fh_locks[i]);
		}
		kfree(ft->fh_locks);
	}

	spinlock_cleanup(&(ft->ssi_spinlock));

	kfree(ft);
}

struct filetable *filetable_copy(struct filetable *ft)
{
	KASSERT(ft != NULL);
	KASSERT(ft->table != NULL);

	struct filetable *result = filetable_create();
	if (result == NULL) {
		return NULL;
	}

	filetable_lock(ft);

	for (int i = 0; i < ft->capacity; i++) {
		if (ft->table[i] != NULL) {
			result->table[i] = ft->table[i];
			filehandle_incref(ft->table[i]);
		}
	}

	filetable_unlock(ft);

	// TODO result->search_start_index will be zero.
	// it's more convinent since if a filehandle at 
	// index fd is in a middle of being destroyed, 
	// it's not gonna get copied to result, but in 
	// the meantime, reuslt->search_start_index will
	// not be equal to min(search_start_index, fd),
	// but will get to be the minimum in ft after
	// the filehandle is destroyed. one solution would
	// be to set it to the minimum explicitly. another
	// is to just leave it as zero and that will still
	// work fine. you may find a better way.

	return result;
}

// table lock should be acquired before calling this function.
int get_min_available_fd(struct filetable *ft, int *fd)
{
	KASSERT(ft != NULL);
	KASSERT(fd != NULL);

	for (int i = ft->search_start_index; i < ft->capacity; i++) {
		if (ft->table[i] == NULL) {

			spinlock_acquire(&(ft->ssi_spinlock));
			ft->search_start_index = i + 1;
			spinlock_release(&(ft->ssi_spinlock));

			*fd = i;

			return 0;
		}
	}

	return EMFILE;
}

// table lock should be acquired before calling this function.
void reclaim_fd(struct filetable *ft, int fd)
{
	KASSERT(fd >= 0);
	KASSERT(ft != NULL);

	spinlock_acquire(&(ft->ssi_spinlock));

	if (fd < ft->search_start_index) {
		ft->search_start_index = fd;
	}

	spinlock_release(&(ft->ssi_spinlock));
}

void filetable_lock_handle(struct filetable *ft, int fd)
{
	KASSERT(fd >= 0);
	KASSERT(ft != NULL);
	KASSERT(ft->table_lock != NULL);
	KASSERT(ft->fh_locks[fd] != NULL);

	// no check for the fd since this is called from 
	// a trusted source.

	// it's fine if the thread is descheduled after only
	// acquiring the rwlock and not the sleep lock since
	// the rwlock does nothing except for indicating that
	// there are threads using the table currently. if 
	// this thread is trying to lock a fh that is already
	// locked, it'll sleep, but no one will be able to 
	// acquire the global rw lock until all locks are 
	// released (including this one that is being waited
	// for). that means that the order in which the 
	// locks are acquired matters.

	rwlock_acquire_read(ft->table_lock);
	lock_acquire(ft->fh_locks[fd]);
}

void filetable_unlock_handle(struct filetable *ft, int fd)
{
	KASSERT(fd >= 0);
	KASSERT(ft != NULL);
	KASSERT(ft->table_lock != NULL);
	KASSERT(ft->fh_locks[fd] != NULL);

	// does the order matter here?
	lock_release(ft->fh_locks[fd]);
	rwlock_release_read(ft->table_lock);
}

void filetable_lock(struct filetable *ft)
{
	KASSERT(ft != NULL);
	KASSERT(ft->table_lock != NULL);

	rwlock_acquire_write(ft->table_lock);
}

void filetable_unlock(struct filetable *ft)
{
	KASSERT(ft != NULL);
	KASSERT(ft->table_lock != NULL);

	rwlock_release_write(ft->table_lock);
}
