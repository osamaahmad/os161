/*
 * Copyright (c) 2001, 2002, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Driver code is in kern/tests/synchprobs.c We will replace that file. This
 * file is yours to modify as you see fit.
 *
 * You should implement your solution to the stoplight problem below. The
 * quadrant and direction mappings for reference: (although the problem is, of
 * course, stable under rotation)
 *
 *   |0 |
 * -     --
 *    01  1
 * 3  32
 * --    --
 *   | 2|
 *
 * As way to think about it, assuming cars drive on the right: a car entering
 * the intersection from direction X will enter intersection quadrant X first.
 * The semantics of the problem are that once a car enters any quadrant it has
 * to be somewhere in the intersection until it call leaveIntersection(),
 * which it should call while in the final quadrant.
 *
 * As an example, let's say a car approaches the intersection and needs to
 * pass through quadrants 0, 3 and 2. Once you call inQuadrant(0), the car is
 * considered in quadrant 0 until you call inQuadrant(3). After you call
 * inQuadrant(2), the car is considered in quadrant 2 until you call
 * leaveIntersection().
 *
 * You will probably want to write some helper functions to assist with the
 * mappings. Modular arithmetic can help, e.g. a car passing straight through
 * the intersection entering from direction X will leave to direction (X + 2)
 * % 4 and pass through quadrants X and (X + 3) % 4.  Boo-yah.
 *
 * Your solutions below should call the inQuadrant() and leaveIntersection()
 * functions in synchprobs.c to record their progress.
 */

#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

int straight_mapping[4] = {3, 0, 1, 2};
int left_mapping 	[4] = {2, 3, 0, 1};

struct lock *locks[4];
struct semaphore *concurrent_cars_sem;

/*
 * Called by the driver during initialization.
 */

void
stoplight_init() {
	locks[0] = lock_create("quadrant one");
	locks[1] = lock_create("quadrant two");
	locks[2] = lock_create("quadrant three");
	locks[3] = lock_create("quadrant four");

	concurrent_cars_sem = sem_create("sync sem", 3);
}

/*
 * Called by the driver during teardown.
 */

void stoplight_cleanup() {
	for (int i = 0; i < 4; i++) {
		lock_destroy(locks[i]);
		locks[i] = NULL;
	}
	sem_destroy(concurrent_cars_sem);
	concurrent_cars_sem = NULL;
}

static
void
enter_first_quadrant(int to_enter, int index)
{
	lock_acquire(locks[to_enter]);

	inQuadrant(to_enter, index);
}

static
void
leave_and_enter(int to_leave, int to_enter, int index)
{
	// if we release first then acquire, the
	// lock of to_leave quadrant will released
	// before calling inQuadrant(to_enter). the
	// to_enter lock may be locked which sends
	// the current thread to sleep with the car
	//  marked to be in the to_leave quadrant and
	//  with the to_leave lock being released.
	//
	// another senario is if the thread gets
	// deschedualed.
	// 
	// must lock(to_enter) then inQuadrant(to_enter)
	// then lock_release(to_leave). step 2 and 3 
	// order cannot be swapped. because if you 
	// swap, the lock of the  to_leave quadrant
	// will be released before calling 
	// inQuadrant(to_enter) and any thread can 
	// grab the lock and enter before you call
	// inQuadrant function.  

	enter_first_quadrant(to_enter, index);

	lock_release(locks[to_leave]);
}

static
void 
leave_last_quadrant(int quadrant, int index)
{
	leaveIntersection(index);
	lock_release(locks[quadrant]);
}

void
turnright(uint32_t direction, uint32_t index)
{
	P(concurrent_cars_sem);

	enter_first_quadrant(direction, index);
	leave_last_quadrant	(direction, index);
	
	V(concurrent_cars_sem);
}

void
gostraight(uint32_t direction, uint32_t index)
{
	P(concurrent_cars_sem);

	enter_first_quadrant(direction,								 						index);
	leave_and_enter    	(direction, 					straight_mapping[direction], 	index);
	leave_last_quadrant	(straight_mapping[direction], 									index);

	V(concurrent_cars_sem);
}

void
turnleft(uint32_t direction, uint32_t index)
{
	// to go left, we go straight, then to the left.

	P(concurrent_cars_sem);

	enter_first_quadrant(direction,									 				  	index);
	leave_and_enter    	(direction, 					straight_mapping[direction], 	index);
	leave_and_enter	   	(straight_mapping[direction], 	left_mapping[direction],		index);
	leave_last_quadrant	(left_mapping[direction], 									  	index);

	V(concurrent_cars_sem);
}
