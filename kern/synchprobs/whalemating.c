/*
 * Copyright (c) 2001, 2002, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Driver code is in kern/tests/synchprobs.c We will
 * replace that file. This file is yours to modify as you see fit.
 *
 * You should implement your solution to the whalemating problem below.
 */

#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

/*
 * Called by the driver during initialization.
 */

struct lock *lock = NULL;

struct cv *male_cv = NULL;
struct cv *female_cv = NULL;
struct cv *matchmaker_cv = NULL;

int male_count = 0;
int female_count = 0;
int matchmaker_count = 0;

void whalemating_init() {
	lock = lock_create("whalemating lock");

	male_cv = cv_create("male");
	female_cv = cv_create("female");
	matchmaker_cv = cv_create("matchmaker");

	male_count = 0;
	female_count = 0;
	matchmaker_count = 0;
}

/*
 * Called by the driver during teardown.
 */

void
whalemating_cleanup() {

	lock_destroy(lock);
	lock = NULL;

	cv_destroy(male_cv);
	cv_destroy(female_cv);
	cv_destroy(matchmaker_cv);

	male_cv = NULL;
	female_cv = NULL;
	matchmaker_cv = NULL;
}

void
male(uint32_t index)
{
	(void)index;
	/*
	 * Implement this function by calling male_start and male_end when
	 * appropriate.
	 */

	male_start(index);

	lock_acquire(lock);
	
	if (matchmaker_count > 0 && female_count > 0) {
		cv_signal(matchmaker_cv, lock);
		cv_signal(female_cv, lock);
	} else {
		male_count++;
		cv_wait(male_cv, lock);
		male_count--;
	}
		
	male_end(index);	

	// releasing the lock after making sure the end function has returned.
	lock_release(lock);

	return;
}

void
female(uint32_t index)
{
	(void)index;
	/*
	 * Implement this function by calling female_start and female_end when
	 * appropriate.
	 */

	female_start(index);

	lock_acquire(lock);
	
	if (matchmaker_count > 0 && male_count > 0) {
		cv_signal(matchmaker_cv, lock);
		cv_signal(male_cv, lock);
	} else {
		female_count++;
		cv_wait(female_cv, lock);
		female_count--;
	}
		
	female_end(index);	

	// releasing the lock after making sure the end function has returned.
	lock_release(lock);

	return;
}

void
matchmaker(uint32_t index)
{
	(void)index;
	/*
	 * Implement this function by calling matchmaker_start and matchmaker_end
	 * when appropriate.
	 */

	matchmaker_start(index);
	
	lock_acquire(lock);
	
	if (male_count > 0 && female_count > 0) {
		cv_signal(male_cv, lock);
		cv_signal(female_cv, lock);
	} else {
		matchmaker_count++;
		cv_wait(matchmaker_cv, lock);
		matchmaker_count--;
	}
		
	matchmaker_end(index);	

	// releasing the lock after making sure the end function has returned.
	lock_release(lock);

	return;
}
