/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Synchronization primitives.
 * The specifications of the functions are in synch.h.
 */

#include <types.h>
#include <lib.h>
#include <spinlock.h>
#include <wchan.h>
#include <thread.h>
#include <current.h>
#include <synch.h>
#include <test.h>

#define __debug__ 0

////////////////////////////////////////////////////////////
//
// Semaphore.

struct semaphore *
sem_create(const char *name, unsigned initial_count)
{
	struct semaphore *sem;

	sem = kmalloc(sizeof(*sem));
	if (sem == NULL) {
		return NULL;
	}

	sem->sem_name = kstrdup(name);
	if (sem->sem_name == NULL) {
		kfree(sem);
		return NULL;
	}

	sem->sem_wchan = wchan_create(sem->sem_name);
	if (sem->sem_wchan == NULL) {
		kfree(sem->sem_name);
		kfree(sem);
		return NULL;
	}

	spinlock_init(&sem->sem_lock);
	sem->sem_count = initial_count;

	return sem;
}

void
sem_destroy(struct semaphore *sem)
{
	KASSERT(sem != NULL);

	/* wchan_cleanup will assert if anyone's waiting on it */
	spinlock_cleanup(&sem->sem_lock);
	wchan_destroy(sem->sem_wchan);
	kfree(sem->sem_name);
	kfree(sem);
}

void
P(struct semaphore *sem)
{
	KASSERT(sem != NULL);

	/*
	 * May not block in an interrupt handler.
	 *
	 * For robustness, always check, even if we can actually
	 * complete the P without blocking.
	 */
	KASSERT(curthread->t_in_interrupt == false);

	/* Use the semaphore spinlock to protect the wchan as well. */
	spinlock_acquire(&sem->sem_lock);
	while (sem->sem_count == 0) {
		/*
		 *
		 * Note that we don't maintain strict FIFO ordering of
		 * threads going through the semaphore; that is, we
		 * might "get" it on the first try even if other
		 * threads are waiting. Apparently according to some
		 * textbooks semaphores must for some reason have
		 * strict ordering. Too bad. :-)
		 *
		 * Exercise: how would you implement strict FIFO
		 * ordering?
		 */
		wchan_sleep(sem->sem_wchan, &sem->sem_lock);
	}
	KASSERT(sem->sem_count > 0);
	sem->sem_count--;
	spinlock_release(&sem->sem_lock);
}

void
V(struct semaphore *sem)
{
	KASSERT(sem != NULL);

	spinlock_acquire(&sem->sem_lock);

	sem->sem_count++;
	KASSERT(sem->sem_count > 0);
	wchan_wakeone(sem->sem_wchan, &sem->sem_lock);

	spinlock_release(&sem->sem_lock);
}

////////////////////////////////////////////////////////////
//
// Lock.

struct lock *
lock_create(const char *name)
{
	struct lock *lock;

	lock = kmalloc(sizeof(*lock));
	if (lock == NULL) {
		return NULL;
	}

	lock->lk_name = kstrdup(name);
	if (lock->lk_name == NULL) {
		kfree(lock);
		return NULL;
	}

	HANGMAN_LOCKABLEINIT(&lock->lk_hangman, lock->lk_name);

	lock->lk_wchan = wchan_create(lock->lk_name);
	if (lock->lk_wchan == NULL) {
		kfree(lock->lk_name);
		kfree(lock);
		return NULL;
	}

	spinlock_init(&lock->lk_lock);
	lock->is_locked = false;

	lock->lk_holder = NULL;

	return lock;
}

void
lock_destroy(struct lock *lock)
{
	KASSERT(lock != NULL);
	KASSERT(!lock->is_locked);
	KASSERT(!lock->lk_holder);

	spinlock_cleanup(&lock->lk_lock);
	wchan_destroy(lock->lk_wchan);

	kfree(lock->lk_name);
	kfree(lock);
}

void
lock_acquire(struct lock *lock)
{
	KASSERT(lock != NULL);

	spinlock_acquire(&lock->lk_lock);

	/* Call this (atomically) before waiting for a lock */
	HANGMAN_WAIT(&curthread->t_hangman, &lock->lk_hangman);

	while (lock->is_locked) {
		wchan_sleep(lock->lk_wchan, &lock->lk_lock);
	}

	KASSERT(!lock->is_locked);

	lock->is_locked = true;
	lock->lk_holder = curthread;

	/* Call this (atomically) once the lock is acquired */
	HANGMAN_ACQUIRE(&curthread->t_hangman, &lock->lk_hangman);

	spinlock_release(&lock->lk_lock);
}

void
lock_release(struct lock *lock)
{
	KASSERT(lock != NULL);
	KASSERT(lock_do_i_hold(lock));

	spinlock_acquire(&lock->lk_lock);

	lock->is_locked = false;
	lock->lk_holder = NULL;
	wchan_wakeone(lock->lk_wchan, &lock->lk_lock);
	
	/* Call this (atomically) when the lock is released */
	HANGMAN_RELEASE(&curthread->t_hangman, &lock->lk_hangman);

	spinlock_release(&lock->lk_lock);
}

bool
lock_do_i_hold(struct lock *lock)
{
	KASSERT(lock != NULL);

	return lock->lk_holder == curthread;
}

////////////////////////////////////////////////////////////
//
// CV


struct cv *
cv_create(const char *name)
{
	struct cv *cv;

	cv = kmalloc(sizeof(*cv));
	if (cv == NULL) {
		return NULL;
	}

	cv->cv_name = kstrdup(name);
	if (cv->cv_name==NULL) {
		kfree(cv);
		return NULL;
	}

	cv->cv_wchan = wchan_create(name);
	if (cv->cv_wchan == NULL) {
		kfree(cv->cv_name);
		kfree(cv);
	}

	spinlock_init(&cv->cv_lock);

	return cv;
}

void
cv_destroy(struct cv *cv)
{
	KASSERT(cv != NULL);

	spinlock_cleanup(&cv->cv_lock);
	wchan_destroy(cv->cv_wchan);

	kfree(cv->cv_name);
	kfree(cv);
}

void
cv_wait(struct cv *cv, struct lock *lock)
{
	KASSERT(cv   != NULL);
	KASSERT(lock != NULL);
	KASSERT(lock_do_i_hold(lock));

	spinlock_acquire(&cv->cv_lock);

	lock_release(lock);
	wchan_sleep(cv->cv_wchan, &cv->cv_lock);
	
	spinlock_release(&cv->cv_lock);

	// can't acquire while holding the spinlock
	// because lock_acquire may sleep
	lock_acquire(lock);
}

void
cv_signal(struct cv *cv, struct lock *lock)
{
	KASSERT(cv   != NULL);
	KASSERT(lock != NULL);
	KASSERT(lock_do_i_hold(lock));

	spinlock_acquire(&cv->cv_lock);

	wchan_wakeone(cv->cv_wchan, &cv->cv_lock);

	spinlock_release(&cv->cv_lock);
}

void
cv_broadcast(struct cv *cv, struct lock *lock)
{
	KASSERT(cv   != NULL);
	KASSERT(lock != NULL);
	KASSERT(lock_do_i_hold(lock));

	spinlock_acquire(&cv->cv_lock);

	wchan_wakeall(cv->cv_wchan, &cv->cv_lock);

	spinlock_release(&cv->cv_lock);
}

////////////////////////////////////////////////////////////
//
// Linked List for rwlock

struct linked_list *
linked_list_create(const char *name)
{
	struct linked_list *ll;
	ll = kmalloc(sizeof(struct linked_list));
	if (ll == NULL) {
		return NULL;	
	}

	ll->name = kstrdup(name);
	if (ll->name == NULL) {
		kfree(ll);
		return NULL;
	}

	ll->tail = kmalloc(sizeof(struct node));
	if (ll->tail == NULL) {
		kfree(ll->name);
		kfree(ll);
		return NULL;
	}
	ll->tail->val = 0;
	ll->tail->next = NULL;

	ll->head = kmalloc(sizeof(struct node));
	if (ll->head == NULL) {
		kfree(ll->name);
		kfree(ll->tail);
		kfree(ll);
		return NULL;
	}
	ll->head->next = ll->tail;

	return ll;
}

static
void
 linked_list_destroy_helper(struct node *n)
{
	if (n == NULL) {
		return;
	}

	linked_list_destroy_helper(n->next);

	kfree(n);
}

void
linked_list_destroy(struct linked_list *ll)
{
	KASSERT(ll != NULL);

	linked_list_destroy_helper(ll->head);
	kfree(ll);
}

void 
linked_list_push_back(struct linked_list *ll, int val)
{
	KASSERT(ll != NULL);
	
	if (ll->tail->val == 0) {
		return;
	}

	ll->tail->next = kmalloc(sizeof(struct node));
	KASSERT(ll->tail->next != NULL);
	ll->tail = ll->tail->next;
	ll->tail->next = NULL;
	ll->tail->val = val;

#if __debug__
	linked_list_print(ll);
#endif

}

void
linked_list_add_back(struct linked_list* ll, int val)
{
	KASSERT(ll != NULL);
	KASSERT(val != 0);
	
	ll->tail->val += val;

#if __debug__
	linked_list_print(ll);
#endif

}

int 
linked_list_pop_front(struct linked_list *ll)
{
	KASSERT(ll != NULL);
	
	int val = ll->head->next->val;

	if (ll->head->next->next == NULL) {
		ll->head->next->val = 0;
	} else {
		struct node *to_be_deleted = ll->head->next;
		ll->head->next = ll->head->next->next;
		kfree(to_be_deleted);
	}

#if __debug__
	linked_list_print(ll);
#endif

	return val;
}

//static
//bool
//linked_list_is_empty(struct linked_list *ll)
//{
//	return ll->head->next->val == 0 && ll->head->next->next == NULL;
//}

static
void
linked_list_print_helper(struct node *node)
{
	if (node == NULL) return;
	
	kprintf_n("%d ", node->val);

	linked_list_print_helper(node->next);
}

void
linked_list_print(struct linked_list *ll)
{
	KASSERT(ll != NULL);
	
	linked_list_print_helper(ll->head->next);
	
	kprintf_n("\n");
}

////////////////////////////////////////////////////////////
//
// rwlock


struct rwlock * 
rwlock_create(const char *name)
{
	KASSERT(name != NULL);

	struct rwlock *rwlock;
	rwlock = kmalloc(sizeof(*rwlock));

	if (rwlock == NULL) {
		return NULL;
	}

	rwlock->rwlock_name = kstrdup(name);
	if (rwlock->rwlock_name == NULL) {
		kfree(rwlock);
		return NULL;
	}

	rwlock->rwlock_lock = lock_create(name);
	if (rwlock->rwlock_lock == NULL) {
		kfree(rwlock->rwlock_name);
		kfree(rwlock);
		return NULL;
	}

	rwlock->readers_cv = cv_create(name);
	if (rwlock->readers_cv == NULL) {
		kfree(rwlock->rwlock_name);
		lock_destroy(rwlock->rwlock_lock);
		kfree(rwlock);
		return NULL;
	}

	rwlock->writers_cv = cv_create(name);
	if (rwlock->writers_cv == NULL) {
		kfree(rwlock->rwlock_name);
		lock_destroy(rwlock->rwlock_lock);
		cv_destroy(rwlock->readers_cv);
		kfree(rwlock);
		return NULL;
	}

	rwlock->current_readers = 0;
	rwlock->writers_waiting = 0;
	rwlock->readers_waiting = 0;
	rwlock->readers_since_last_write = 0;
	rwlock->writing_mode = false;
	
	return rwlock;	
}

void rwlock_destroy(struct rwlock *rwlock)
{
	KASSERT(rwlock != NULL);
	KASSERT(rwlock->current_readers == 0);
	KASSERT(rwlock->writers_waiting == 0);
	KASSERT(rwlock->readers_waiting == 0);
	KASSERT(rwlock->writing_mode == false);
	
	kfree(rwlock->rwlock_name);
	lock_destroy(rwlock->rwlock_lock);
	cv_destroy(rwlock->readers_cv);
	cv_destroy(rwlock->writers_cv);
	kfree(rwlock);
}

static
int
calc_allowed_readers_count(struct rwlock* rwlock)
{
	if (rwlock->writers_waiting == 0) 
		return rwlock->readers_waiting;
	
	int ratio = rwlock->readers_waiting / rwlock->writers_waiting;

	if (ratio == 0) ratio++;	

	int limit = ratio - rwlock->readers_since_last_write;

	if (limit <= 0) return 0;
	return limit;
}

void rwlock_acquire_read(struct rwlock *rwlock)
{
	KASSERT(rwlock != NULL);

	lock_acquire(rwlock->rwlock_lock);
	
	KASSERT(rwlock->current_readers >= 0);
	KASSERT(rwlock->readers_waiting >= 0);
	KASSERT(rwlock->writers_waiting >= 0);
	KASSERT(rwlock->readers_since_last_write >= 0);

	rwlock->readers_waiting++;

	if (rwlock->writing_mode || rwlock->writers_waiting > 0) {
		while (rwlock->writing_mode || calc_allowed_readers_count(rwlock) == 0) {
			cv_wait(rwlock->readers_cv, rwlock->rwlock_lock);
		} 
	}

	rwlock->readers_waiting--;
	rwlock->current_readers++;
	rwlock->readers_since_last_write++;

	lock_release(rwlock->rwlock_lock);
}

void rwlock_release_read(struct rwlock *rwlock)
{
	KASSERT(rwlock != NULL);

	lock_acquire(rwlock->rwlock_lock);

	rwlock->current_readers--;

	KASSERT(rwlock->current_readers >= 0);
	KASSERT(rwlock->readers_waiting >= 0);
	KASSERT(rwlock->writers_waiting >= 0);

	if (rwlock->current_readers == 0) {
		cv_signal(rwlock->writers_cv, rwlock->rwlock_lock);
	}

	lock_release(rwlock->rwlock_lock);
}

void rwlock_acquire_write(struct rwlock *rwlock)
{
	KASSERT(rwlock != NULL);

	lock_acquire(rwlock->rwlock_lock);

	rwlock->writers_waiting++;

	while (rwlock->writing_mode || rwlock->current_readers > 0) {
		cv_wait(rwlock->writers_cv, rwlock->rwlock_lock);
	}
	
	rwlock->writing_mode = true;
	rwlock->writers_waiting--;
	rwlock->readers_since_last_write = 0;
	
	KASSERT(rwlock->current_readers >= 0);
	KASSERT(rwlock->readers_waiting >= 0);
	KASSERT(rwlock->writers_waiting >= 0);

	lock_release(rwlock->rwlock_lock);
}

void rwlock_release_write(struct rwlock *rwlock)
{
	// in case readers are way faster than writers, 
	// all writers will wait, then all readers will
	// form a single node in rwlock->readers_waiting.
	// this will make it unfair for the writers since
	// they may come first, but they'll have to wait
	// for all the readers to finish first.

	KASSERT(rwlock != NULL);

	lock_acquire(rwlock->rwlock_lock);

	KASSERT(rwlock->writing_mode);

	rwlock->writing_mode = false;

	int readers_to_wake = calc_allowed_readers_count(rwlock);

	if (readers_to_wake == 0) {
		cv_signal(rwlock->writers_cv, rwlock->rwlock_lock);
	} else {
		for (int i = 0; i < readers_to_wake; i++) {
			cv_signal(rwlock->readers_cv, rwlock->rwlock_lock);
		}
	}
	
	KASSERT(rwlock->current_readers >= 0);
	KASSERT(rwlock->readers_waiting >= 0);
	KASSERT(rwlock->writers_waiting >= 0);
	
	lock_release(rwlock->rwlock_lock);
}
