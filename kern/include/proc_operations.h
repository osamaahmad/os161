#include <types.h>
#include <cdefs.h>
#include <mips/trapframe.h>
#include <proc.h>

#define ALIGNMENT_SIZE 4

#define _ARG_MAX ARG_MAX
#define _PATH_MAX PATH_MAX

int   sys_fork(struct trapframe *, pid_t *);
int   sys_execv(const_userptr_t, userptr_t);
int   sys_waitpid(pid_t, userptr_t, int, int *);
void  sys__exit(int);
void _sys__exit(int);
pid_t sys_getpid(int *);
