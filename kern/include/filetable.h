#pragma once

#include <spinlock.h>
#include <synch.h>
#include <vnode.h>
#include <limits.h>

#define FILEHANDLE_ISSEEKABLE (1 << 2)
#define FILETABLE_SIZE OPEN_MAX

// File Handle -------------------------

struct filehandle {
	int flags;
	off_t offset;
	unsigned int refcount;
	struct vnode *vnode_ptr;
	struct lock *offset_lock;
	struct spinlock refcount_spinlock;

	// flags:
	//     bit 0: O_RDONLY or O_WRONLY
	//     bit 1: O_RDWR
	//     bit 2: is_seekable
};

struct filehandle *filehandle_create(void);
void filehandle_destroy(struct filehandle *);

void filehandle_incref(struct filehandle *);
int  filehandle_decref(struct filehandle *);

// File Table --------------------------

struct filetable {
	struct filehandle **table;
	struct lock **fh_locks;
	struct rwlock *table_lock;

	// search start index spinlock
	struct spinlock ssi_spinlock;

	// TODO consider using a type other than int.

	int search_start_index;
	int capacity;
};

struct filetable *filetable_create(void);
void filetable_destroy(struct filetable *);

struct filetable *filetable_copy(struct filetable *);

int get_min_available_fd(struct filetable *, int *);
void reclaim_fd(struct filetable *, int);

void filetable_lock_handle(struct filetable *, int);
void filetable_unlock_handle(struct filetable *, int);
void filetable_lock(struct filetable *);
void filetable_unlock(struct filetable *);
