#include <types.h>
#include <filetable.h>

int  sys_open_nocheck(struct filetable *, char *, int, mode_t, int *);
int  sys_open(userptr_t, int, mode_t, int *);
void sys_close_nocheck_nolock(struct filetable *, int);
int  sys_close(int);
int  sys_write_nocheck(struct filehandle *, void *, size_t, ssize_t *);
int  sys_write(int, const_userptr_t, size_t, ssize_t *);
int  sys_read_nocheck(struct filehandle *, void *, size_t, ssize_t *);
int  sys_read(int, userptr_t, size_t, ssize_t *);
int  sys_lseek_nocheck(struct filehandle *, off_t, int, off_t *);
int  sys_lseek(int, int32_t, int32_t, userptr_t, int32_t *, int32_t *);
int  sys_dup2(int, int, int *);
int  sys_dup2_nocheck(struct filetable *, int, int);
int  sys_chdir(userptr_t);
int  sys___getcwd(userptr_t, size_t, int *);
