/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Simple program to add two numbers (given in as arguments). Used to
 * test argument passing to child processes.
 *
 * Intended for the basic system calls assignment; this should work
 * once execv() argument handling is implemented.
 */

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <test161/test161.h>

int nargs = 4;
const char *arr[] = { "A", "B", "C", "D" };
      int  cnt1[] = {  2 ,  4 ,  8 ,  16 };
      int  cnt2[] = {  2 ,  4 ,  8 ,  16 };

static void print1(int fd, int depth)
{
	for (int i = 2; depth < nargs; i <<= 1, depth++) {
		for (int j = 0; j < i; j++) {
			ssize_t len = write(fd, arr[depth], 1);
			if (len != 1) {
				printf("Error writing to the file with fd=%d, len=%d.\n", fd, len);
				exit(-1);
			}
		}
	}
}

static void print2(int fd, int depth)
{
	if (depth == nargs) {
		return;
	}

	pid_t pid = fork();

	if (pid == -1) {
		printf("Error forking.\n");
		exit(-1);
	}

	if (write(fd, arr[depth], 1) != 1) {
		printf("Error writing to the file.\n");
		exit(-1);
	}

	print2(fd, depth + 1);

	if (pid) {
		waitpid(pid, 0, 0);
	} else {
		exit(0);
	}
}

static void check(int fd, const char *name)
{
	printf("\n%s results:\n", name);

	char ch;

	while (1) {

		if (read(fd, &ch, 1) < 1) {
			break;
		}

		cnt1[ch - 'A']--;

		// really?..
		if (write(STDOUT_FILENO, &ch, 1) != 1) {
			printf("Error writing to the stdout.\n");
			exit(-1);
		}
	}

	ch = '\n';
	write(STDOUT_FILENO, &ch, 1);

	for (int i = 0; i < nargs; i++) {
		if (cnt1[i] != 0) {
			printf("Error: only %d occurences of the letter %s. Expected %d.\n", 
				cnt2[i] - cnt1[i], arr[i], cnt2[i]);
		}
	}
}

static void set_cnt()
{
	for (int i = 0; i < nargs; i++) {
		cnt1[i] = cnt2[i];
	}
}

int
main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	for (int i = 0; i < nargs; i++) {
		printf("Expected %d * %s (%d of %s).\n", cnt1[i], arr[i], cnt2[i], arr[i]);
	}

	int fd1r = open("hello1.txt", O_RDONLY          , 0644);
	int fd1w = open("hello1.txt", O_WRONLY | O_CREAT, 0644);

	int fd2r = open("hello2.txt", O_RDONLY          , 0644);
	int fd2w = open("hello2.txt", O_WRONLY | O_CREAT, 0644);

	set_cnt();
	print1(fd1w, 0);
	check(fd1r, "loop");

	set_cnt();
	print2(fd2w, 0);
	check(fd2r, "fork");

}
